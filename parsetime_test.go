package question2

import (
	"testing"
	"time"
)

func TestMonth(t *testing.T) {
	const longForm = "Jan 2, 2006 at 3:04pm (MST)"
	time, _ := time.Parse(longForm, "Feb 3, 2013 at 7:54pm (PST)")
	t.Run("Check the parser", func(t *testing.T) {
		got := Month(time)
		want := "19:54:0 2"
		if got != want {
			t.Errorf("got : %v but want : %v", got, want)
		}
	})
}
