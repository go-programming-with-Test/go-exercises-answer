// Go program to illustrate
// the execution of default case
package main

import "fmt"

// Main function
func main() {

	// Creating a channel
	var c chan int

	select {
	case x1 := <-c:
		fmt.Println("Value: ", x1)
	default:
		fmt.Println("Default case..!")
	}
}
