package question6

import (
	"bytes"
	"strconv"
	"strings"
	"testing"
)

func TestSendDataAsJson(t *testing.T) {
	t.Run("Testing_with_parsing_arguments", func(t *testing.T) {
		want := 2
		got, err := strconv.Atoi(strings.TrimSpace(Selected("ResultCode", "resultcode")))
		if err != nil {
			t.Fatal("can not convert to int ", err.Error())
		}
		if want != got {
			t.Errorf("want %d and got %d ", want, got)
		}
	})

	t.Run("Testing_Sending_to_server", func(t *testing.T) {
		want := "success"
		buf := &bytes.Buffer{}

		SendConvertedJson(buf, copiedJson.Password)
		got := buf.String()
		if want != got {
			t.Errorf("want %s and got %s ", want, got)
		}

	})

}
