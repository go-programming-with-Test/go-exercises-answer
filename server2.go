package question6

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
)

type C2BPaymentQueryResultCopy struct {
	ResultCode    int    `json: "ResultCode" xml: "ResultCode"`
	ResultDesc    string `json: "ResultDesc" xml: "ResultDesk"`
	TransID       int    `json: "TransID" xml: "TransID"`
	Password      string `json: "Password" xml: "Password"`
	BillRefNumber int    `json: "BillRefNumber" xml: "BillRefNumber"`
	UtilityName   string `json: "UtilityName" xml: "UtilityName"`
	CustomerName  string `json: "CustomerName" xml: "CustomerName"`
	Amount        int    `json: "Amount" xml: "Amount"`
}

func Select(key string, xmlstring string) string {
	resultCode := regexp.MustCompile("<" + key + ">(.|\n)*?</" + key + ">")
	result := resultCode.FindString(xmlstring)
	result = strings.ReplaceAll(result, "<"+key+">", "")
	result = strings.ReplaceAll(result, "</"+key+">", "")
	return result
}

var copiedJson C2BPaymentQueryResultCopy
var passcode = "cGFzc3dvcmQ="

var fileToWrite = fmt.Sprintf(`
	  <soapenv:Header/> 
	  <soapenv:Body> 
		 <c2b:C2BPaymentQueryResult> 
			<ResultCode>%d</ResultCode> 
			<ResultDesc>%s</ResultDesc> 
			<TransID>%d</TransID>
			<Password>{{%s}}</Password>
			<BillRefNumber>%d</BillRefNumber> 
			<UtilityName>%s</UtilityName> 
			<CustomerName>%s</CustomerName> 
			<Amount>%d</Amount> 
		 </c2b:C2BPaymentQueryResult> 
	  </soapenv:Body> 
	</soapenv:Envelope>
	`, copiedJson.ResultCode, copiedJson.ResultDesc, copiedJson.TransID, copiedJson.Password, copiedJson.BillRefNumber,
	copiedJson.UtilityName, copiedJson.CustomerName, copiedJson.Amount)

func HandleHttpRequest(w http.ResponseWriter, r *http.Request) {
	err := json.NewDecoder(r.Body).Decode(&copiedJson)
	if err != nil {
		log.Fatal(err)
		fmt.Fprint(w, "Unable to read the data", http.StatusBadRequest)
	}
}

// a function that matches the encoded password and create an xml file of success and fail
func AcceptJsonFileofSucess(pattern, password string) {
	path.Match("cGFzc3dvcmQ=", "cGFzc3dvcmQ=")
	file, err := os.OpenFile("sucess.xml", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	scanner := bufio.NewScanner(file)
	newFile := make([]string, 0)
	for scanner.Scan() {
		line := scanner.Text()
		newFile = append(newFile, line)
	}
	if err != nil {
		log.Fatal("Can not create file", err)
	}
	_, err = file.WriteString(fileToWrite)
	if err != nil {
		log.Fatal("We can not write into the file", err)
		fmt.Printf("unable to save the file ", http.StatusInternalServerError)
	}

	defer filepath.Ext("sucess.xml")
}

// If server 2 sends http status other than 200, then print a log "Server 2 failed to acknowledge".

func AcceptJsonFileofFailure() {
	path.Match("cGFzc3dvcmQ=", "password")
	file2, err := os.OpenFile("failed.xml", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	scan := bufio.NewScanner(file2)
	newFile := make([]string, 0)
	for scan.Scan() {
		line := scan.Text()
		newFile = append(newFile, line)
	}
	if err != nil {
		log.Fatal("Can not create file", err)
		fmt.Printf("Unable to save the file ", http.StatusInternalServerError)
	}
	if err != nil {
		log.Fatal("Sorry, We can not write into the file", err)
	}
	defer filepath.Ext("failed.xml")
	return
}
