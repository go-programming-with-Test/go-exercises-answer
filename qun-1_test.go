package question1

import (
	"reflect"
	"testing"
)

// given each company profits with thier ID
func TestCompanyProfit(t *testing.T) {
	company1 := &CompanyProfit{
		ID:     "ID 1",
		Profit: 1000,
	}
	company2 := &CompanyProfit{
		ID:     "ID 2",
		Profit: 1000,
	}
	company3 := &CompanyProfit{
		ID:     "ID 2",
		Profit: 1000,
	}
	company4 := &CompanyProfit{
		ID:     "ID 4",
		Profit: 1000,
	}
	// total company profits
	testcases := []struct {
		Name  string
		Want  []CompanyTotalProfit
		Input []CompanyProfit
	}{

		{Name: "single company", Want: []CompanyTotalProfit{{CompanyID: "ID 1", TotalProfit: 1000}}, Input: []CompanyProfit{*company1}},
		{Name: "two company", Want: []CompanyTotalProfit{{CompanyID: "ID 1", TotalProfit: 1000}, {CompanyID: "ID 2", TotalProfit: 1000}}, Input: []CompanyProfit{*company1, *company2}},
		{"Multiple company test", []CompanyTotalProfit{{CompanyID: "ID 1", TotalProfit: 1000}, {CompanyID: "ID 2", TotalProfit: 2000}}, []CompanyProfit{*company1, *company2, *company3}},
		{"4 company test", []CompanyTotalProfit{{CompanyID: "ID 1", TotalProfit: 1000}, {CompanyID: "ID 2", TotalProfit: 2000}, {CompanyID: "ID 4", TotalProfit: 1000}}, []CompanyProfit{*company1, *company2, *company3, *company4}},
	}
	for _, test := range testcases {
		t.Run(test.Name, func(t *testing.T) {
			got := CalculateProfits(test.Input)
			if !reflect.DeepEqual(got, test.Want) {
				t.Errorf("got %+v, but want %+v ", got, test.Want)
			}

		})
	}
}
