package question2

// create a function to properly parse a time in the following format. (hours, minutes, seconds month )

import (
	"fmt"
	"time"
)

func Month(t time.Time) string {
	parsetime := fmt.Sprintf("%d:%d:%d %d", (t.Hour()), (t.Minute()), (t.Second()), t.Month())

	return parsetime
}
