package question1

import (
	"fmt"
	"time"
)

type CompanyProfit struct {
	ID         string
	Department string
	Profit     float64
	StartTime  time.Time
	EndTime    time.Time
}
type CompanyTotalProfit struct {
	CompanyID   string
	TotalProfit float64
}

func CalculateProfits(profits []CompanyProfit) []CompanyTotalProfit {
	var result []CompanyTotalProfit
	var filteredID = make(map[string]float64)
	// map will give unique company id wit value 0 as a total profit
	for _, company := range profits {
	  filteredID[company.ID] = 0
	}
	fmt.Printf("Comapny ID%v\n", filteredID) // for cheking
  
	for key := range filteredID { // loop over those unique company ids that are key of the map
	  for _, profit := range profits { // loop each profit of the company
		if profit.ID == key {
		  filteredID[key] += profit.Profit // sum the profit from 0
  
		}
	  }
	  //append the totla profit
	  result = append(result, CompanyTotalProfit{CompanyID: key, TotalProfit: filteredID[key]})
  
	}
  
	return result
}
