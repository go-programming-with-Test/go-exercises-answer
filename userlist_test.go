package question3

import (
	"reflect"
	"testing"
	"time"
)

var UnixDate = "Mon Jan _2 15:04:05 MST 2006"
var RFC822 = "02 Jan 06 15:04 MST"

func TestGroupUsersByWeek(t *testing.T) {
	week1, _ := time.Parse(RFC822, "02 Jan 06 15:04 MST")
	User1 := &User{
		ID:      "ID1",
		Created: week1,
	}
	week2, _ := time.Parse(UnixDate, "Mon Jan _2 15:04:05 MST 2006")
	User2 := &User{
		ID:      "ID2",
		Created: week2,
	}
	week3, _ := time.Parse(UnixDate, "Mon Jan _2 15:04:05 MST 2006")
	User3 := &User{
		ID:      "ID3",
		Created: week3,
	}
	week4, _ := time.Parse(UnixDate, "Mon Jan _2 15:04:05 MST 2006")
	User4 := &User{
		ID:      "ID4",
		Created: week4,
	}
	week5, _ := time.Parse(UnixDate, "Mon Jan _2 15:04:05 MST 2006")
	User5 := &User{
		ID:      "ID5",
		Created: week5,
	}
	week6, _ := time.Parse(UnixDate, "Mon Jan _2 15:04:05 MST 2006")
	User6 := &User{
		ID:      "ID6",
		Created: week6,
	}
	week7, _ := time.Parse(UnixDate, "Mon Jan _2 15:04:05 MST 2006")
	User7 := &User{
		ID:      "ID7",
		Created: week7,
	}
	data1 := &CreatedWeek{
		WeekNumber: 1,
		Users:      []User{*User1},
	}
	data2 := &CreatedWeek{
		WeekNumber: 2,
		Users:      []User{*User2},
	}
	data3 := &CreatedWeek{
		WeekNumber: 3,
		Users:      []User{*User3},
	}
	data4 := &CreatedWeek{
		WeekNumber: 4,
		Users:      []User{*User4},
	}
	data5 := &CreatedWeek{
		WeekNumber: 5,
		Users:      []User{*User5},
	}
	data6 := &CreatedWeek{
		WeekNumber: 6,
		Users:      []User{*User6},
	}
	data7 := &CreatedWeek{
		WeekNumber: 7,
		Users:      []User{*User7},
	}
	// format: created week- week num n user, User id n created time.time
	runtest := []struct {
		want     []CreatedWeek
		username []User
	}{
		{[]CreatedWeek{{data1.WeekNumber, data1.Users}}, data1.Users},
		{[]CreatedWeek{{data2.WeekNumber, data2.Users}}, data2.Users},
		{[]CreatedWeek{{data3.WeekNumber, data3.Users}}, data3.Users},
		{[]CreatedWeek{{data4.WeekNumber, data4.Users}}, data4.Users},
		{[]CreatedWeek{{data5.WeekNumber, data5.Users}}, data5.Users},
		{[]CreatedWeek{{data6.WeekNumber, data6.Users}}, data6.Users},
		{[]CreatedWeek{{data7.WeekNumber, data7.Users}}, data7.Users},
	}
	for _, test := range runtest {

		got := GroupUsersByWeek(test.username)
		if !reflect.DeepEqual(got, test.want) {

			t.Errorf("got %v but want %v", got, test.want)
		}
	}
}
