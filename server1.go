package question6

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
)

var xmlString = ` 
<soapenv:Header/> 
<soapenv:Body> 
   <c2b:C2BPaymentQueryResult> 
	  <ResultCode>2</ResultCode> 
	  <ResultDesc>Failed</ResultDesc> 
	  <TransID>10111</TransID>
	  <Password>{{encrypted_password}}</Password>
	  <BillRefNumber>12233</BillRefNumber> 
	  <UtilityName>sddd</UtilityName> 
	  <CustomerName>wee</CustomerName> 
	  <Amount>30</Amount> 
   </c2b:C2BPaymentQueryResult> 
</soapenv:Body> 
</soapenv:Envelope>`

var password = "password"

// a sended JSON file have password under C2BPaymentQueryResult field
type C2BPaymentQueryResult struct {
	ResultCode    int    `xml: "ResultCode" json: "ResultCode"`
	ResultDesc    string `xml: "ResultDesc" json: "ResultDesk"`
	TransID       int    `xml: "TransID" json: "TransID"`
	Password      string `xml: "Password" json: "Password"`
	BillRefNumber int    `xml: "BillRefNumber" json: "BillRefNumber"`
	UtilityName   string `xml: "UtilityName" json: "UtilityName"`
	CustomerName  string `xml: "CustomerName" json: "CustomerName"`
	Amount        int    `xml: "Amount" json: "Amount"`
}

// use scrypt key derivation function
func EncryptPassword(password string) string {
	Encoding := base64.StdEncoding.EncodeToString([]byte(password))
	return Encoding
}

func Selected(key string, xmlstring string) string {
	resultCode := regexp.MustCompile("<" + key + ">(.|\n)*?</" + key + ">")
	result := resultCode.FindString(xmlstring)
	result = strings.ReplaceAll(result, "<"+key+">", "")
	result = strings.ReplaceAll(result, "</"+key+">", "")
	return result
}

// function that convert xmlstring to json
func ConvertXmlstringToJson(cj C2BPaymentQueryResult) []byte {
	resultCode := (cj.ResultCode)
	resultDesc := cj.ResultDesc
	transID := cj.TransID
	password := EncryptPassword(password)
	billRefNumber := cj.BillRefNumber
	utilityName := cj.UtilityName
	customerName := cj.CustomerName
	amount := cj.Amount

	data := C2BPaymentQueryResult{ResultCode: resultCode, ResultDesc: resultDesc,
		TransID: transID, Password: password, BillRefNumber: billRefNumber,
		UtilityName: utilityName, CustomerName: customerName, Amount: amount}
	result, err := json.Marshal(data)
	if nil != err {
		fmt.Println("Error marshalling to JSON", err)
	}
	return result
}

var c2b = ConvertXmlstringToJson(C2BPaymentQueryResult{})

// Send converted file to server2 and have time out of request
func SendConvertedJson(w io.Writer, file string) {
	url := "http://127.0.0.1:8080"
	request, err := http.NewRequest("POST", url, bytes.NewBuffer(c2b))
	if err != nil {
		log.Fatal("Error Can not create request", err.Error())
		os.Exit(1)
	}
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	if err != nil {
		log.Fatal("can not read the body", err)
		os.Exit(1)
	}

	if response.StatusCode == http.StatusTooManyRequests || response.StatusCode == http.StatusNotFound {
		SendConvertedJson(os.Stdout, Selected(xmlString, password))
		// use context time out condition
	}
	if response.StatusCode != http.StatusOK {
		SendConvertedJson(w, "unable to send converted file")
		os.Exit(1)
	}
	if response.StatusCode == int(http.StatusOK) {
		SendConvertedJson(w, "success")
	}
}
