// how deadlock arises
package main

// Main function
func main() {

	// Creating a channel
	// Here deadlock arises because
	// no goroutine is writing
	// to this channel so, the select
	// statement has been blocked forever
	c := make(chan int)
	select {
	case <-c:
	}
}

// We get an output of: 
	// fatal error: all goroutines are asleep - deadlock!
